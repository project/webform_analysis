(function ($, Drupal, settings) {
  /**
   * Webform Analysis - Charts.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.WebformAnalysisCharts = {
    attach(context, settings) {
      if (!(context instanceof HTMLDocument)) {
        return;
      }

      const webformcharts = settings.webformcharts;

      if (webformcharts === undefined) {
        return;
      }

      google.charts.load('current', { packages: webformcharts.packages });

      google.charts.setOnLoadCallback(function () {
        const charts = $.map(webformcharts.charts, function (value, index) {
          return [value];
        });

        charts.forEach(function (chart) {
          const data = new google.visualization.arrayToDataTable(chart.data);
          const options = chart.options;
          const gchart = new google.visualization[chart.type](
            document.querySelector(chart.selector),
          );
          gchart.draw(data, options);
        });
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
