<?php

namespace Drupal\webform_analysis;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webform\WebformInterface;

/**
 * Defines the handler for the webform analysis entity type.
 */
class WebformAnalysis implements WebformAnalysisInterface {

  use StringTranslationTrait;

  /**
   * The webform variable.
   *
   * @var \Drupal\webform\WebformInterface
   */
  protected $webform;

  /**
   * The entity variable.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The entity variable.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $sourceEntity;

  /**
   * The elements variable.
   *
   * @var array
   */
  protected $elements;

  /**
   * WebformAnalysis Constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity of form.
   * @param string $field_name
   *   (optional) The webform field name to display. Required if $entity is not
   *   a Webform.
   */
  public function __construct(EntityInterface $entity, $field_name = NULL) {
    if ($entity instanceof WebformInterface) {
      $this->webform = $entity;
      $this->entity = NULL;
    }
    else {
      /** @var \Drupal\webform\WebformRequestInterface $webform_request */
      // @phpstan-ignore-next-line as it is used on purpose.
      $webform_request = \Drupal::service('webform.request');
      [$this->webform, $this->sourceEntity] = $webform_request->getWebformEntities();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getWebform() {
    return $this->webform;
  }

  /**
   * {@inheritdoc}
   */
  public function setComponents(array $components = []) {
    $this->webform->setThirdPartySetting('webform_analysis', 'components', $components);
  }

  /**
   * {@inheritdoc}
   */
  public function getComponents() {
    return (array) $this->webform->getThirdPartySetting('webform_analysis', 'components');
  }

  /**
   * {@inheritdoc}
   */
  public function setChartType($chart_type = '') {
    $this->webform->setThirdPartySetting('webform_analysis', 'chart_type', $chart_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getChartType() {
    return (string) $this->webform->getThirdPartySetting('webform_analysis', 'chart_type');
  }

  /**
   * {@inheritdoc}
   */
  public function getElements() {
    if (!$this->elements) {
      $this->elements = $this->webform->getElementsInitializedFlattenedAndHasValue();
    }
    return $this->elements;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentValuesCount($component) {
    // @phpstan-ignore-next-line as this class object used at several places.
    $query = \Drupal::database()->select('webform_submission_data', 'wsd');
    $query->fields('wsd', ['value']);
    $query->addExpression('COUNT(value)', 'quantity');
    $query->leftJoin('webform_submission', 'ws', 'wsd.sid = ws.sid');
    $query->condition('wsd.webform_id', $this->webform->id());
    $query->condition('name', $component);
    if ($this->entity) {
      $query->condition('entity_type', $this->entity->getEntityTypeId());
      $query->condition('entity_id', $this->entity->id());
    }
    if (!empty($this->getStartDate())) {
      $query->condition('ws.completed', $this->getStartDate()->getTimestamp(), '>=');
    }
    if (!empty($this->getEndDate())) {
      $query->condition('ws.completed', $this->getEndDate()->getTimestamp(), '<=');
    }
    $query->groupBy('wsd.value');
    $records = $query->execute()->fetchAll();

    $values = [];
    $allNumeric = TRUE;
    foreach ($records as $record) {
      if (is_numeric($record->value)) {
        $value = $this->castNumeric($record->value);
      }
      else {
        $value = $record->value;
        $allNumeric = FALSE;
      }
      $values[$value] = (int) $record->quantity;
    }

    if ($allNumeric) {
      ksort($values);
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentRows($component, array $header = [], $value_label_with_count = FALSE) {
    $rows = [];
    foreach ($this->getComponentValuesCount($component) as $value => $count) {
      $type = $this->getElements()[$component]['#type'];
      $value_label = '';
      switch ($type) {

        // Sets Yes/No labels for webform elements of type Checkbox.
        case 'checkbox':
          $value_label = $value ? $this->t('Yes') : $this->t('No');
          break;

        // Sets Term names labels for Term type webform elements.
        case 'webform_term_select':
        case 'webform_term_checkboxes':
          if (isset($value) && !empty($value)) {
            $entity_storage = \Drupal::entityTypeManager()
              ->getStorage('taxonomy_term');
            $entity = $entity_storage->load($value);
            if (is_object($entity)) {
              $value_label = $entity->label();
            }
          }

        // Sets Entity labels for Entity type webform elements.
        case 'webform_entity_radios':
        case 'webform_entity_select':
        case 'webform_entity_checkboxes':
          if (isset($value) && !empty($value)) {
            if (isset($this->getElements()[$component]['#target_type'])) {
              $target_type = $this->getElements()[$component]['#target_type'];
              $entity_storage = \Drupal::entityTypeManager()
                ->getStorage($target_type);
              $entity = $entity_storage->load($value);
              if (is_object($entity)) {
                $value_label = $entity->label();
              }
            }
          }
          break;

        // Sets element name as labels for other elements.
        default:
          $value_label = $this->getElements()[$component]['#options'][$value] ?? $value;
          break;
      }
      if ($value_label_with_count && $value_label != '') {
        $value_label .= ' : ' . $count;
      }
      if ($value_label) {
        $rows[] = [(string) $value_label, $count];
      }
    }
    if ($header && $rows) {
      array_unshift($rows, $header);
    }

    return $rows;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentTitle($component) {
    if (!isset($this->getElements()[$component]['#title'])) {
      return $component;
    }
    return $this->getElements()[$component]['#title'];
  }

  /**
   * {@inheritdoc}
   */
  public static function getChartTypeOptions() {
    return [
      ''            => t('Table'),
      'PieChart'    => t('Pie Chart'),
      'ColumnChart' => t('Column Chart'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isInt($i = '') {
    return ($i === (string) (int) $i);
  }

  /**
   * {@inheritdoc}
   */
  public function castNumeric($i = '') {
    if (empty($i)) {
      return 0;
    }

    return $this->isInt($i) ? $i : "{$i}";
  }

  /**
   * {@inheritdoc}
   */
  public function setStartDate(int $date) {
    $this->webform->setThirdPartySetting('webform_analysis', 'start_date', $date);
  }

  /**
   * {@inheritdoc}
   */
  public function getStartDate() {
    $timestamp = $this->webform->getThirdPartySetting('webform_analysis', 'start_date');
    if (!empty($timestamp)) {
      return DrupalDateTime::createFromTimestamp($timestamp);
    }
    return $timestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndDate(int $date) {
    $this->webform->setThirdPartySetting('webform_analysis', 'end_date', $date);
  }

  /**
   * {@inheritdoc}
   */
  public function getEndDate() {
    $timestamp = $this->webform->getThirdPartySetting('webform_analysis', 'end_date');
    if (!empty($timestamp)) {
      return DrupalDateTime::createFromTimestamp($timestamp);
    }
    return $timestamp;
  }

}
